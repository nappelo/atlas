from django.contrib.auth import views as auth_views
from django.urls import path, re_path, include
from rest_framework import routers
from homepage import views, viewclasses
from webservice import viewsets

app_name = 'homepage'

api_router = routers.DefaultRouter()
api_router.register(r'maps', viewsets.MapViewSet, basename='maps')
api_router.register(r'layers', viewsets.LayerViewSet, basename='layers')

urlpatterns = [
    path('help', views.v3_help, name='v3_help'),
    path('disclaimer', views.v3_disclaimer, name='v3_disclaimer'),
    path('login', viewclasses.LoginView.as_view(template_name='v3/login.html'), name='v3_login'),
    path('login/failure', views.v3_login_failure, name='v3_login_failure'),
    path('logout', auth_views.LogoutView.as_view(template_name='v3/logout.html'), name='v3_logout'),
    path('admin2/', views.v3_admin, name='v3_admin'),
    path('api/v1/token', views.v3_token, name='v3_token'),
    path('api/v1/', include(api_router.urls)),
    re_path('embed', views.embed, name='embed'),
    re_path(r'((?P<theme_slug>[a-z0-9\-]+)?)', views.v3, name='v3'),
]
